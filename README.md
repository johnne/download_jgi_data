# Downloading data from JGI

To download data from JGI you can use the rules found in download_jgi.rules. 

1. Add your JGI username and password to separate lines of the jgi_login file.
2. Edit the jgi_configuration.yaml file and add the portal_name to be accessed.
3. [Optional] Comment out certain directories under dir_structure in the config file to exclude those directories from download.
4. Run snakemake:
```bash
snakemake 
```