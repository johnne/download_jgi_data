configfile: "jgi_configuration.yaml"

import hashlib
assert (config["portal_name"]),"ERROR: portal_name field in configuration cannot be left empty!"

rule all:
    input:
        fileinfo=config["file_info"]

rule download_file_listing:
  output:
    filelist=temp("files.xml"),
    cookies=temp("cookies")
  run:
    username = ""
    password = ""
    portal = config["portal_name"]
    with open(config["login_info"]) as fh:
      for i, line in enumerate(fh):
        if i == 0: username = line.rstrip()
        else: password = line.rstrip()
    shell("echo Downloading file list")
    shell("curl -sS 'https://signon.jgi.doe.gov/signon/create' --data-urlencode 'login={username}' --data-urlencode 'password={password}' -c {output.cookies} > /dev/null")
    shell("curl -sS 'https://genome.jgi.doe.gov/ext-api/downloads/get-directory?organism={portal}' -b {output.cookies} > {output.filelist}")

def get_md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

rule download_files:
  input:
    filelist="files.xml",
    cookies="cookies"
  output:
    status="download.status",
    fileinfo=config["file_info"]
  run:
    import xml.etree.ElementTree as ET
    import pandas as pd, re
    tree = ET.parse(input.filelist)
    root = tree.getroot()
    files = {}
    for folder in tree.findall("folder"):
      try:
        output_folder = config["dir_structure"][folder.attrib["name"]]
      except KeyError:
        continue
      shell("mkdir -p {output_folder}")

      for file in folder.findall("file"):
        if config["file_suffixes"]:
          dl = False
          for suffix in config["file_suffixes"]:
            if re.search(suffix,file.attrib["filename"]):
              dl = True
              break
        else:
          dl = True
        if dl:
          files[output_folder+"/"+file.attrib["filename"]] = file.attrib

    status = {}
    file_info = {}
    # Download files and check md5sum
    total_files = len(files)
    file_number = 0
    for filename,attrib in files.items():
      file_info[filename] = {"label":attrib["label"],"project":attrib["project"],"library":[attrib["library"]]}
      for key,value in file_info[filename].items():
        if value == ['']: file_info[filename][key] = ''
      url,md5 = attrib["url"],attrib["md5"]
      url = url.replace("&amp","&")
      file_number+=1
      if os.path.isfile(filename):
        md5_local = get_md5(filename)
        if md5_local == md5:
          shell("echo {filename} already exists")
          continue
      shell("echo Downloading {filename} {file_number}/{total_files}")
      shell("curl -sS 'https://genome.jgi.doe.gov/{url}' -b {input.cookies} > {filename}")
      #print("Checking md5sum...",end="")
      md5_local = get_md5(filename)
      if md5_local == md5:
        status[filename] = "passed"
      else:
        status[filename] = "failed"
      shell("echo {filename} "+status[filename])


    with open(output.status, 'w') as fh:
      for filename,status in status.items():
        fh.write(filename+":"+status+"\n")

    df = pd.DataFrame(file_info).T
    df.index.name = "filename"
    df.to_csv(output.fileinfo, sep="\t")
